﻿using System.Net.Http.Json;

namespace OtusWebConsole
{
    internal class Program
    {
        static HttpClient httpClient = new HttpClient();
        static async Task Main(string[] args)
        {
            var user = new User()
            {
                Email = "test@test.com",
                FirstName = "Nick",
                LastName = "Petrov"
            };

            using (var response = await httpClient.PostAsJsonAsync("https://localhost:7001/api/user/", user))
            {

            }


            //    StringContent content = new StringContent("{{\"firstName\" : \"AA\",\"lastName\" : \"AA\",\"email\" : \"AA\"}}");
            //var response = await client.PostAsync("https://localhost:7001/api/User", content);
            //string responseText = await response.Content.ReadAsStringAsync();
            //Console.WriteLine(responseText);

            //var result = await client.GetStringAsync("https://localhost:7001/swagger/index.html");

            //Console.WriteLine(result);  
            //client.Dispose();
            //Console.ReadKey();

        }
    }
}
