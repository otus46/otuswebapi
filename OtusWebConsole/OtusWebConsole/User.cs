﻿using System.ComponentModel.DataAnnotations;

namespace OtusWebConsole
{
    public class User
    {
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

    }
}
